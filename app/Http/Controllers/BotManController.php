<?php

namespace App\Http\Controllers;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Slack\SlackDriver;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle ()
    {
        $botman = app('botman');


        $botman->hears('hey', function (Botman $bot) {
            $bot->reply('Howdy Partner!');
        });

        $botman->hears('o meu nome é {nome}', function (Botman $bot, $nome) {
            $bot->reply('Estou ao seu dispor ' . $nome);
        });

        $botman->group(['driver' => SlackDriver::class], function(Botman $bot) {
            $bot->hears('ola', function(Botman $bot) {
                $bot->reply('Estou aqui!!');
            });
        });

        $botman->hears('Olá', function(Botman $bot) {
            $bot->startConversation(new BemVindoConversation);
        });

        $botman->hears('Onde', function(Botman $bot) {
            $bot->startConversation(new SpeakingLaravelConversation);
        });

        $botman->listen();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker ()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation (BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }
}

class BemVindoConversation extends Conversation{

    protected $primeiroNome;

    protected $email;

    public function pedirPrimeiroNome()
    {
        $this->ask('Olá! Qual é o seu primeiro nome?', function(Answer $resposta) {
            // Save result
            $this->primeiroNome = $resposta->getText();

            $this->say('É um prazer conhecê-lo '.$this->primeiroNome);
            $this->pedirEmail();
        });
    }

    public function pedirEmail()
    {
        $this->ask('Poderia-me facultar o seu e-mail?', function(Answer $resposta) {
            // Save result
            $this->email = $resposta->getText();

            $this->say('Excelente, agora podemos começar '.$this->primeiroNome);
        });
    }


    public function run()
    {
        $this->pedirPrimeiroNome();

        $this->askForDatabase();
    }
}

class SpeakingLaravelConversation extends Conversation {

    public function giveOptions()
    {
        $question = Question::create('Diz-me tu :sunglasses:')
            ->fallback('oopsss ...perdi-me na tua resposta')
            ->addButtons([
                Button::create('Speaking About Laravel')->value('yes'),
                Button::create('Algures perdido no espaço...')->value('no'),
            ]);

    }

    public function run(){
        $this->giveOptions();
    }
}
